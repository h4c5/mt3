import numpy as np
import math
import pandas as pd
import pytest
import sys

from importlib import reload

from mt3 import thompson


def test_import(monkeypatch: pytest.MonkeyPatch):
    """Test if we can import package without numpy and pandas"""
    with monkeypatch.context() as m:

        # import mt3 when pandas is not available pandas
        m.setitem(sys.modules, "pandas", None)
        reload(thompson)

        # import mt3 when pandas is not available pandas
        m.setitem(sys.modules, "numpy", None)
        reload(thompson)

    reload(thompson)


def test_mean():
    """Test mean calculation
    Example serie : 2, 4, 4, 4, 5, 5, 7, 9
    Reference for example:
        https://en.wikipedia.org/wiki/Standard_deviation
    """
    serie = [2, 4, 4, 4, 5, 5, 7, 9]
    serie_nan = [2, 4, 4, 4, 5, 5, 7, 9, np.nan]

    mean = 5

    assert thompson.mean(serie) == mean
    assert thompson.mean(np.array(serie)) == mean
    assert thompson.mean(pd.Series(serie)) == mean

    assert np.isnan(thompson.mean(np.array(serie_nan)))
    assert np.isnan(thompson.mean(pd.Series(serie_nan)))

    assert (
        thompson.mean(pd.Series(serie_nan), where=~pd.Series(serie_nan).isna()) == mean
    )

    assert (
        thompson.mean(
            serie, where=[False, True, True, True, False, False, False, False]
        )
        == 4
    )


def test_std():
    """Test standard deviation calculation

    Example serie : 2, 4, 4, 4, 5, 5, 7, 9
    mean : 5
    sum of squarred deviation: 32
    variance : 32 / 8 = 4
    standard deviation : sqrt(32 / 8) = 2
    standard deviation with ddof = 1 : sqrt(32 / 7) ~= 2.14


    Reference for example:
        https://en.wikipedia.org/wiki/Standard_deviation
    """
    serie = [2, 4, 4, 4, 5, 5, 7, 9]
    serie_nan = [2, 4, 4, 4, 5, 5, 7, 9, np.nan]

    std = pytest.approx(2.14, 0.01)
    std_ddof0 = 2

    assert thompson.std(serie) == std
    assert thompson.std(np.array(serie)) == std
    assert thompson.std(pd.Series(serie)) == std

    assert np.isnan(thompson.std(np.array(serie_nan)))
    assert np.isnan(thompson.std(pd.Series(serie_nan)))

    assert thompson.std(pd.Series(serie_nan), where=~pd.Series(serie_nan).isna()) == std

    assert thompson.std(np.array(serie), ddof=0) == std_ddof0
    assert thompson.std(pd.Series(serie), ddof=0) == std_ddof0

    assert thompson.std(serie, sample_mean=0) == math.sqrt(
        sum(s**2 for s in serie) / (len(serie) - 1)
    )

    assert (
        thompson.std(serie, where=[False, True, True, True, False, False, False, False])
        == 0
    )


def test_modified_thompson_tau_test_step():
    """Test modified_thompson_tau_test_step with list, numpy arrays and pandas Series"""
    serie_outlier = [-4, 3, -5, -2, 0, 1, 1000]
    conf_level = 0.99

    # Test with a list. Should return True, 5 since it iterate over values
    assert thompson.modified_thompson_tau_test_step(serie_outlier, conf_level) == 6

    # Test with an array. Should return True, 5 since it iterate over values
    assert (
        thompson.modified_thompson_tau_test_step(np.array(serie_outlier), conf_level)
        == 6
    )

    # Test with an Series. Should return True, 5 since it iterate over values
    assert (
        thompson.modified_thompson_tau_test_step(
            pd.Series(serie_outlier, index=[1, 5, 2, 0, 4, 6, 3]), conf_level
        )
        == 6
    )

    where = [True, True, True, True, True, True, False]
    assert (
        thompson.modified_thompson_tau_test_step(serie_outlier, conf_level, where=where)
        == None
    )


def test_modified_thompson_tau_test():
    """Test modified_thompson_tau_test with list, numpy arrays and pandas Series"""
    serie_outlier = [-4, 3, -5, -2, 0, 1, 1000]
    serie_outlier_nan = [-4, np.nan, 3, -5, -2, 0, 1, 1000]
    conf_level = 0.99

    expected_result = [False, False, False, False, False, False, True]
    expected_result_nan = [False, False, False, False, False, False, False, True]
    expected_result_nan_out = [False, True, False, False, False, False, False, True]

    # Test for a list sample
    assert (
        thompson.modified_thompson_tau_test(serie_outlier, conf_level)
        == expected_result
    )

    # Test for a numpy.ndarray sample
    assert (
        thompson.modified_thompson_tau_test(np.array(serie_outlier), conf_level)
        == np.array(expected_result)
    ).all()

    # Test for a pandas.Series sample
    assert (
        thompson.modified_thompson_tau_test(pd.Series(serie_outlier), conf_level)
        == np.array(expected_result)
    ).all()

    # Test with nan value
    assert (
        thompson.modified_thompson_tau_test(np.array(serie_outlier_nan), conf_level)
        == np.array(expected_result_nan)
    ).all()

    # Test with nan value and nan are outliers
    assert (
        thompson.modified_thompson_tau_test(
            np.array(serie_outlier_nan), conf_level, nan_is_outlier=True
        )
        == np.array(expected_result_nan_out)
    ).all()
