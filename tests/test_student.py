import math
import pytest
import sys

from importlib import reload

from mt3 import student


def test_get_student_t_crit_value(monkeypatch: pytest.MonkeyPatch):
    """Test of get_student_t_crit_value with and without scipy (optional dependency)"""
    STUDENT_T_CRITS = student.STUDENT_T_CRITS

    # Testing first, last and infinite value
    # cf https://www.itl.nist.gov/div898/handbook/eda/section3/eda3672.htm
    assert STUDENT_T_CRITS[0.9][0] == 3.078
    assert STUDENT_T_CRITS[0.9][-2] == 1.29
    assert STUDENT_T_CRITS[0.9][-1] == 1.282

    # Testing first value
    assert student.get_student_t_crit_value(1, 0.9) == pytest.approx(
        STUDENT_T_CRITS[0.9][0], 0.001
    )

    # Testing last value
    assert student.get_student_t_crit_value(
        len(STUDENT_T_CRITS[0.9]), 0.9
    ) == pytest.approx(STUDENT_T_CRITS[0.9][-2], 0.001)

    # Testing "infinite" value
    assert student.get_student_t_crit_value(1e3, 0.9) == pytest.approx(
        STUDENT_T_CRITS[0.9][-1], 0.001
    )

    with monkeypatch.context() as m:
        # Disable scipy
        m.setitem(sys.modules, "scipy.stats", None)
        reload(student)

        # values returned by get_student_t_crit_value should not have change
        assert (
            student.get_student_t_crit_value(1, 0.9)
            == STUDENT_T_CRITS[0.9][0]
        )

        assert (
            student.get_student_t_crit_value(1e3, 0.9)
            == STUDENT_T_CRITS[0.9][-1]
        )

        # Now we should get a KeyError for conf_level 0.91 since it is not in
        # STUDENT_T_CRITS
        with pytest.raises(KeyError):
            student.get_student_t_crit_value(1, 0.91)


def test_get_rejection_threshold():
    """Test get_rejection_threshold"""
    n = 3
    conf_level = 0.9

    t_crit = 3.078  # Student T critical value for df = n - 2 = 1 and conf_level = 0.9

    treshold = (t_crit * (n - 1)) / (math.sqrt(n) * math.sqrt(n - 2 + (t_crit**2)))

    assert student.get_rejection_threshold(n, conf_level) == pytest.approx(
        treshold, 0.001
    )
